//console.log(document);


// DOM Manipulation

const txtFirstName = document.querySelector("#txt-first-name");
const txtLastName = document.querySelector("#txt-last-name");
const spanFullName = document.querySelector("#span-full-name");
console.log(txtFirstName);
console.log(txtLastName);
console.log(spanFullName);

txtFirstName.addEventListener('keyup', printFirstName);
txtLastName.addEventListener('keyup', printLastName);


function printFirstName (event) {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
}

function printLastName (event) {
	spanFullName.innerHTML = txtFirstName.value + " " + txtLastName.value
}

txtFirstName.addEventListener('keyup', (event) => {
	console.log(event)
	console.log(event.target)
	console.log(event.target.value)
})

txtLastName.addEventListener('keyup', (event1) => {
	console.log(event1)
	console.log(event1.target)
	console.log(event1.target.value)
})

/*
	The "event" argument contains the information on the triggered event.
	The "event.target" contains the element where the event happened 
	The "event.target.value" gets the value of the input object (similar to the txtFirstName.value).
*/


const labelFirstName = document.querySelector("#label-txt-fname");

labelFirstName.addEventListener('click', (e) => {
	console.log(e)
	alert("You clicked the First Name Label.")
})

const labelLastName = document.querySelector("#label-txt-lname");

labelLastName.addEventListener('click', (e) => {
	console.log(e)
	alert("You clicked the First Name Label.")
})
